README.txt
==========

Allows users to create & display Common Asked Question content and display as Accordian whereever then want show in the website.
------------------------
Requires - Drupal 8


Overview:
--------
Adds content in the content type, view 

The styling is minimal grey and easily over writeable by developers.


INSTALLATION:
--------
1. Install & Enable the module
2. Add Content  > Add COntent > Common Asked Questions
3. View and blcok is created automatically named as Common Asked Questions
4. Assign the Block whcih region you want to show.
5. Call the block or region in your desired theme file

Support requests
----------------
You can request support here: https://www.drupal.org/project/issues/common_asked_questions/

